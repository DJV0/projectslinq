﻿using Console_API.Entities;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Console_API
{
    class DataService
    {
        private readonly HttpClient _client;
        public DataService()
        {
            _client = new HttpClient();
        }
        public async Task<List<Project>> GetData()
        {
            var projects = await GetEntitiesByUriInternal<Project>("https://bsa21.azurewebsites.net/api/projects");
            var tasks = await GetEntitiesByUriInternal<Entities.Task>("https://bsa21.azurewebsites.net/api/tasks");
            var teams = await GetEntitiesByUriInternal<Team>("https://bsa21.azurewebsites.net/api/teams");
            var users = await GetEntitiesByUriInternal<User>("https://bsa21.azurewebsites.net/api/users");

            return projects
                .Join(users.Join(teams, user => user.TeamId, team => team.Id, (user, team) => new User
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    BirthDay = user.BirthDay,
                    Email = user.Email,
                    RegisteredAt = user.RegisteredAt,
                    TeamId = user.TeamId,
                    Team = team
                }),
                project => project.AuthorId,
                user => user.Id, (project, user) => new Project
                {
                    Id = project.Id,
                    Name = project.Name,
                    Description = project.Description,
                    Deadline = project.Deadline,
                    CreatedAt = project.CreatedAt,
                    AuthorId = project.AuthorId,
                    Author = user,
                    TeamId = project.TeamId
                })
                .Join(teams, project => project.TeamId, team => team.Id, (project, team) => new Project
                {
                    Id = project.Id,
                    Name = project.Name,
                    Description = project.Description,
                    Deadline = project.Deadline,
                    CreatedAt = project.CreatedAt,
                    AuthorId = project.AuthorId,
                    Author = project.Author,
                    TeamId = project.TeamId,
                    Team = team
                })
                .GroupJoin(
                    tasks
                        .Join(users.Join(teams, user => user.TeamId, team => team.Id, (user, team) => new User
                                                                                        {
                                                                                            Id = user.Id,
                                                                                            FirstName = user.FirstName,
                                                                                            LastName = user.LastName,
                                                                                            BirthDay = user.BirthDay,
                                                                                            Email = user.Email,
                                                                                            RegisteredAt = user.RegisteredAt,
                                                                                            TeamId = user.TeamId,
                                                                                            Team = team
                                                                                        }),
                                task => task.PerformerId, user => user.Id, (task, user) => new Entities.Task
                                {
                                    Id = task.Id,
                                    Name = task.Name,
                                    Description = task.Description,
                                    State = task.State,
                                    CreatedAt = task.CreatedAt,
                                    FinishedAt = task.FinishedAt,
                                    PerformerId = task.PerformerId,
                                    Performer = user,
                                    ProjectId = task.ProjectId,

                                })
                        .Join(projects, task => task.ProjectId, project => project.Id, (task, project) => new Entities.Task
                        {
                            Id = task.Id,
                            Name = task.Name,
                            Description = task.Description,
                            State = task.State,
                            CreatedAt = task.CreatedAt,
                            FinishedAt = task.FinishedAt,
                            PerformerId = task.PerformerId,
                            Performer = task.Performer,
                            ProjectId = task.ProjectId,
                            Project = project
                        }),
                    project => project.Id,
                    task => task.ProjectId,
                    (project, tasks) => new Project
                    {
                        Id = project.Id,
                        Name = project.Name,
                        Description = project.Description,
                        Deadline = project.Deadline,
                        CreatedAt = project.CreatedAt,
                        AuthorId = project.AuthorId,
                        Author = project.Author,
                        TeamId = project.TeamId,
                        Team = project.Team,
                        Tasks = tasks
                    })
                .ToList();
        }

        private async Task<List<T>> GetEntitiesByUriInternal<T>(string requestUri)
        {
            var response = await _client.GetStringAsync(requestUri);
            var result = JsonConvert.DeserializeObject<List<T>>(response);
            return result;
        }
    }
}
